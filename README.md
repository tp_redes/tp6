# Configuraciones Kubernetes
## Instalación y configuración
### Instalación
```bash
sudo snap install microk8s --classic
```
### Configuración firewall
Habilitar la comunicación pod2pod y pod2net
```bash
sudo ufw allow in on cni0 && sudo ufw allow out on cni0
sudo ufw default allow routed
```

## Configuraciones microk8s
Habilitar addons necesarios
```bash
microk8s enable <addon>
```
- addons:
    1. dns
    2. ingress
    3. storage (en caso de usar MongoDB con Statefulsets)

## Configuración de variable de entorno para *kubectl*
Permite la ejecución privilegiada de ***kubectl*** sin necesidad de anteponer **sudo**
```bash
sudo usermod -a -G microk8s <username>
sudo chown -f -R <username> ~/.kube
```
Permite la ejecución del comando ***kubectl*** sin necesidad de precederlo por el comando ***microk8s***.
```bash
snap alias microk8s.kubectl kubectl
```
Carga toda la configuración de kubernetes en el entorno del usuario, incluídos los certificados de snap.
```bash
kubectl config view --raw > $HOME/.kube/config
```
## Configuraciones para ruteo IPv4 e IPv6
Como primera medida se debe habilitar el el reenvío para IPv6 en la VM de los hosts con el siguiente comando:
```bash
sudo sysctl -w net.ipv6.conf.all.forwarding=1
```
Una vez que tenemos habilitado, podemos probar que los paquetes viajen correctamente entre hosts de distintas redes.
Luego, para la comunicación entre las VMs se debe habilitar el modo primiscuo en las interfaces de la VM y del router.
```bash
sudo ip link set <interfazVM> promisc on # Normalmente suele ser enp0s3
docker exec -it r1 ip link set <interfazALaVM> promisc on # Normalmente suele ser eth1
```
Habilitado el modo promiscuo, se debe definir el default gateway para IPv4 en la VM donde corre k8s.
Se deben tener 2 adaptadores de red en cada VM, uno en modo **NAT** y otro en modo **puente** con el modo promiscuo activado.
El que configuraremos será el puente, que normalmente se encuenta en la interfaz ***enp0s8***.

**Se debe esperar hasta que Strapi esté levantado, ya que al setear el default gateway se corta la conexión a internet**.
```bash
sudo ifconfig enp0s8 <ipv4> netmask 255.255.255.0 up # Debe estar en la misma red que la interfaz del router que une las VMs
sudo ip route add default via <ipv4DeLaInterfazDelRouter>
```
* En caso de caerse Strapi, se debe eliminar el default gateway incluído, apagar y levantar ***microk8s*** y volver a intentar.

Para el caso de IPv6 se deben crear las interfaces primero y además setear el default gateway:
```bash
sudo ip -6 address add 2000::11/64 dev <interfazDeLaVM>
sudo ip -6 route add default via 2000::10
```
Chequear que funcione haciendo ping.
## Configuración de variables de entorno para TLS
Lo primero que hay que hacer es setear las siguientes variables de entorno:
```bash
HOST=<host.com> # URL del server (e.g: appstrapi.com)
CERT_FILE=<cert.crt> # nombre.crt
KEY_FILE=<file.key> # nombre.key
```
Una vez seteadas, correr el siguiente comando para generar el certificado SSL:
```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ${KEY_FILE} -out ${CERT_FILE} -subj "/CN=${HOST}/O=${HOST}"
```
Creado el certificado, se debe insertar en k8s en forma de ***secret*** usando el siguiente comando:
```bash
kubectl create secret tls <nombreDelSecret> --key ${KEY_FILE} --cert ${CERT_FILE} # Debe coincidir con el indicado en el ingress
```
Luego, podemos descargar el certificado para incluirlo en los hosts alpine y utilizarlo para la conexión segura de la siguiente forma:
```bash
openssl s_client -showcerts -connect appstrapi.com:443 </dev/null 2>/dev/null|openssl x509 -outform PEM > appstrapi-com.pem
```
